def contient(ls :tuple, lettre : str) -> bool :
    if ls[1] == lettre:
        return True
    elif est_feuille(ls):
        contient(ls[-1], lettre)
    else:
        contient(ls[0], lettre)

ar : tuple = (((None,'o',None),'l',((None,'h',None),'r',None)),'a',(((None,'m',None),'i',(None,'e',None)),'g',(None,'t',None)))


contient(ar, 'a')