l = [[4], [3], [8], [2], [7], [1], [5]]


def interclassement(l1, l2):
    ln = []
    n1, n2 = len(l1), len(l2)
    i1, i2 = 0, 0
    while i1 < n1 and i2 < n2:
        if l1[i1] < l2[i2]:
            ln.append(l1[i1])
            i1 += 1
        else:
            ln.append(l2[i2])
            i2 += 1
    return ln + l1[i1:] + l2[i2:]


def fusion(l):
    if len(l) <= 1:
        return l
    m = len(l)//2
    return interclassement (fusion(l[:m]), fusion(l[m:]))

print(fusion(l))