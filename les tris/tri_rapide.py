from random import randint


def tri_rapide(liste: list):
    if not liste:
        return []
    else:
        pivot = liste[-1]
        plus_petit = [x for x in liste if x <  pivot]
        plus_grand = [x for x in liste[:-1] if x >= pivot]
        return tri_rapide(plus_petit) + [pivot] + tri_rapide(plus_grand)

l = []

for i in range(20):
    l.append(randint(0,155))
print(l)

print(tri_rapide(l))