import turtle as trt
from faker import Faker
fake = Faker()
trt.color('#3f2020')
trt.bgcolor(fake.color(luminosity='light'))

def trace(p, l, a):
    '''
    Trace l'arbre
    p correspond à la profondeur de l'arbre,
    l à la longueur des branches,
    a à l'angle entre chaque branches
    '''
    if p==0: #On est au bout des branches, 
        trt.color('green') # On trace donc les feuilles, d'où le changement de couleur
        trt.fd(l) 
        trt.backward(l) 
        trt.color('#3f2020') # on reprend la couleur de l'arbre
    else:
        trt.width(p)
        trt.fd(l/3) 
        trt.lt(a) 
        trace(p-1,l*2/3, a) # On trace un nouvel arbre plus petit à la gauche de la dernière branche
        trt.rt(2*a) 
        trace(p-1,l*2/3, a) # On trace un nouvel arbre plus petit à la droite de la dernière branche
        trt.lt(a) 
        trt.backward(l/3) # recule pour revenir en bas des branches, pour pouvoir tracer la branche de l'autre coté


def arbre(p,l,a):
    '''
    Configure turtle pour que l'arbre soit bien tracé, 
    càd que l'on déplace turtle pour qu'il soit centré et dans la bonne direction.
    les arguments sont les mêmes que ceux de la fonction arbre
    '''
    trt.speed(0)
    trt.hideturtle() 
    trt.up() # lève le stylo
    trt.rt(90)  
    trt.fd(280) 
    trt.lt(180) 
    trt.down() # baisse le stylo pour que turtle puisse écrire
    trace(p, l, a) # exécute le tracé
    trt.mainloop() 
    
    
arbre(9, 700, 71)
