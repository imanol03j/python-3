from random import randint
import turtle as trt

def fougere(p):
    trt.speed(0)
    trt.penup()
    trt.ht() # = hideturtle
    x = 0
    y = 0
    for i in range(p):  # Il faut mettre un grande nombre car sinon la fougère ne sera pas complète / détaillé
        trt.goto(65 * x, 37 * y - 252) # va à la coord calculé
        trt.pendown()
        trt.dot(2.5, "green")          # et y place un point
        trt.penup()
        r = randint(0, 100) # sort une probabilité, une coord sera calculé en fonction de la probabilité et un point sera placé
        if r < 1:   # sort 1% du temps                                                                          #   aux coord
            x = 0
            y = 0.16 * y + 1.6
        elif r < 87:     # sort 86 %
            x = 0.85 * x + 0.04 * y
            y = -0.04 * x + 0.85 * y + 1.6
        elif r < 94:    # sort 7% du temps
            x = 0.2 * x - 0.26 * y
            y = 0.23 * x + 0.22 * y + 1.6
        else:   # sort 6 % du temps 
            x = -0.15 * x + 0.28 * y
            y = 0.26 * x + 0.24 * y + 0.44
            
        
        



fougere(10000000)
