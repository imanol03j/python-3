# Projet fractale
par Imanol Jordana


## Fractale :
___
C'est un objet géométrique « infiniment morcelé » dont des détails sont     observables à une échelle arbitrairement choisie. En zoomant sur 
une partie de la figure, il est possible de retrouver toute la figure ; on dit alors   qu’elle est « auto-similaire ».

## Histoire des fractales :

La notion de fractale mathématiques apparaît aux 17ème siècle pour représenter des structures simples auto-similaire. Cette notion apparaît 
grâce à la recursivité. Il découlera des fractals les concept de dimensions fractales et de paterne géométrique naturel.
___
### J'ai décider de définir deux types de fractale:

1. Les fractales géométriques
2. Les fractales arithmetiques

Les fractales géométriques sont celles qui se crée grâce à des formes géométriques, sans calculs.

Les fractales arithmetiques sont donc celles qui sont crée à partir de formules mathématiques.

### 1. Les Fractales géométriques

J'ai choisi pour illustrer ce concept de travailler sur un arbre fractale :

![Arbre](./img/arbre.png)

Le principe est que chaque branche est l'arbre en plus petit.
Le moyen le plus simple est de le réaliser grâce au principe de récursivité.

Ce sont les fractales qui sont dans l'idée les plus simples à réaliser, car généralement elles sont justes basées sur le principes de 
récurrence. Mais qui garantisse un résultat avec un logiciel adapté à l'affichage de fractals.

### 2. Les Fractales arithmetiques

Les fractales arithmetiques sont généralement les plus intéressantes car elles sont créer à partir d'une formule mathematique plus ou moins
complexe. Pour garder cet idée, pour en réaliser une avec Turtle de Python, il faut utiliser le jeu du chaos. C'est une méthode inventée par 
Barnsley pour représenter des fractales. La fractale est créée en créant, par itérations successives une séquence de points, partant d'un 
point initial choisi aléatoirement, pour lesquels chaque point de la séquence est positionné à une fraction donnée de la distance qui sépare 
le point précédent d'un des sommets du polygone. Ce sommet est choisi aléatoirement à chaque itération. En répétant ce processus un nombre de 
fois important. 

Pour représenter ces fractales j'ai utiliser la fougère de Barnsley :

![Fougère_Début](./img/fougere_dbt.png)*

La fougère de Barnsley, va être créer grâce au jeu du chaos et à la matrice suivante :

![matrice](./img/matrice.png)


Une nombre aléatoire sera tiré, represantant une probabilité, et en fonction de la sorti une des 4 matrices sera calculée pour placer un
point. C'est là qu'arrive le problème de Turtle pour représenter des fractales. Les calculs matriciels de ce programme sont assez simple mais pour que la fougère soit complète il faut répéter l'action un nombre ridiculement grand (entre 150 milles et 1 million), or Turtle est lent pour faire des action répétitives.

Le programme n'utilise par explissitement les matrices, mais justes ses données, car avec des calculs aussi simple, l'utilisation de matrice, avec par exemple Numpay, rend le tout plus long et moins lisible : on double le nombre de lignes attribués au calcul.

