from math import exp

def f(x):
    return(x*exp(x))

def dichotomie (a, b, c):
    if b-a < 0.1 :
        return a
    while b - a > 0.1:
        m = (a+b)/2
        if f(m)<c:
            return(dichotomie (m, b, c))
        else :
            return(dichotomie (a, m, c))

print(dichotomie(-1, 3, 60))