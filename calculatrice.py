## Importations
import operator as operator


## Classes
class Maillon():

    def __init__(self, val) -> None:
        self._val = val
        self._suiv = None

    def get_val(self) :
        return self._val 

    def get_suiv(self):
        return self._suiv

    def set_suiv(self, m) -> None:
        if m is None or (type(m._val) == type(self._val)):
            self._suiv = m

    def __repr__(self) -> str:
        return f"[{self._val}]-->{None if self._suiv is None else self._suiv._val}"


class Pile():
    
    def __init__(self: "Pile") -> None:
        self._sommet = None 

    def est_vide(self: "Pile") -> bool:
        return self._sommet is None

    def empiler(self: "Pile", val) -> None:
        s: Maillon = Maillon(val)
        if self._sommet is None:
            self._sommet = Maillon(val)
        else:
            s.set_suiv(self._sommet)
            self._sommet = s

    def depiler(self):
        if self._sommet is None:
            raise IndexError("La pile est vide ou ne comporte qu'un seul nombre, on ne peut donc pas appliquer un operateur")
        else:
            s = self._sommet.get_val()
            self._sommet = self._sommet.get_suiv()
            return(s)
        

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self._sommet}-|"



### Les différents opérateurs
operations = {
    '+':  operator.add, '-':  operator.sub,
    '*':  operator.mul, '/':  operator.truediv,
    '%':  operator.mod, '**': operator.pow,
    '//': operator.floordiv
}


### Programme Principale
pile = Pile()


entrée = 0
print ("opérateur spéciaux : \n % -> retourne le reste de la division \n ** -> fais le carré \n // division arrondie \n\n Ecrire stop pour finir le calcul \n") 
while not entrée == "stop":
        entrée = input("Rentrer un nombre ou un opérateur :")
        print(entrée)
        for operateurs in operations.keys():
            if operateurs == entrée:
                un = int(pile.depiler())
                deux = int(pile.depiler())
                resultat = operations[operateurs](un,deux)
                entrée = str(resultat)
        pile.empiler(entrée)
        print(pile)

