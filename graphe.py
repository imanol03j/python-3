personnes = {'Ana' : ['Bob'], 'Bob' : ['Ana','Erol'], 'Carl': ['Bob'], 'Erol': ['Bob', 'Dora', 'Filo', 'Gab'], 'Filo' : ['Gab'], 'Dora' : ['Gab'], 'Gab': ['Erol']}

def amis_d_amis(dico, pers):
    resultat = []
    amis = dico[pers]
    for ami in amis:
        for a in dico[ami]:
            if a != pers and a not in resultat :
                resultat.append(a)
    resturn resultat