from typing import DefaultDict


mat = [
        [0, 5, 3, 0, 4], 
        [5, 0, 0, 0, 2], 
        [3, 0, 0, 4, 2], 
        [0, 0, 4, 0, 7], 
        [4, 2, 2, 7, 0]]

def exist(mat: list, lst: list)->bool:
    for i in range(len(lst)-1):
        if mat[lst[i]][lst[i+1]] == 0 :
            return False
        return True
    
print(exist(mat, [0, 1, 4, 2]))

def longueur_pacome(mat, lst):
    re = True
    p = 0
    s = lst[0]
    for suiv in lst[1:]:
        re = re & bool(mat[s][suiv])
        p = p + mat[s][suiv]
        s = suiv
    return re * p

def longueur_louis(mat, lst)-> int:
    total = 0
    for i, a in enumerate(lst[:1]):
        b = lst[i+1]
        long = mat[a][b]
        if long == 0:
            return 0
        total += long
    return total


def longueur_ethan(mat, lst)->int:
    longgueur = 0
    for i in range(len(lst)-1):
        if mat[lst[i]][lst[i+1]] == 0:
            return 0
        else:
            longueur += mat[lst[i]][lst[i+1]]
        return longueur