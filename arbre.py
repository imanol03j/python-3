from random import randint

def nœud():

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
        self.parent = None

    def __str__(self):
        return self.data

    def insert(self, data):
        if data < self.data:
            if self.left is None:
                self.left = nœud(data)
                self.left.parent = self
            else:
                self.left.insert(data)
        elif data > self.data:
            if self.right is None:
                self.right = nœud(data)
                self.right.parent = self
            else:
                self.right.insert(data)


a = nœud()

for i in range(20):
    a.insert(randint(0,250))
print(a)


