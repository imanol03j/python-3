from maPile import *

def parenthesage(s: str) -> bool: 
    p: Pile[str] = Pile()
    for carac in s:
        if carac == '(':
            p.empiler(carac)
        elif carac == ')':
            if p.est_vide():
                return False
            else :
                p.depiler()
        else:
            return False
    return p.est_vide()


def parenthèse(série: str) -> bool :
    compte = 0

    for carac in range(len(série)):
        if série[carac] == '(' :
            compte += 1
        else :
            compte -= 1
    if compte != 0:
        p_test = False
    else :
        p_test = True

    if série[0] == ')' or série[-1] == '(' :
        return False
    elif p_test == False:
        return False
    return True


    assert parenthèse('()()')
    assert not parenthèse('(()))')
    assert not parenthèse(')(')
    assert not parenthèse('(((((((((((((((((((((((((((((((')
    assert not parenthèse('(hey)')
    

print(parenthèse('()'))