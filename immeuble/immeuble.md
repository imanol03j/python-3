# Projet immeuble

Par Imanol JORDANA

Ce programme dessine une rue d'immeuble de taille et de couleur variable.

Ce programme utilise :
    
- *Turtle*
- *random*
- *typing*

## Valeurs et couleurs

Les valeurs et les couleurs qui vont être utilisé tout au long du programme sont prédéfinis dès le début 

Ainsi si l'on veut modifier certaines valeur/mesures/couleurs, on peut le faire qu'une fois au début et on aura donc pas besoin de modifié la valeur appelée tout au long du programme.

## Fonction

### va 

On deplace le crayon turtle au coordonées. Le crayon va se déplacer en étant levé , comme ça il ne fait pas un trait de son point de 
départ jusqu'à sa position d'arrivée.

### rectangle

Cet fonction permet de créer un rectangle de toute dimension, ainsi on peut l'utiliser regulierement tout au long du programme.

### fenetre

Grâce à la fonction rectangle, on dessine un carré qui sert de fenetre.

### porte

Grâce à la fonction rectangle, on fait une porte qui va servir de porte d'entré mais également de porte-fenetre.

### balcon

On créer un balcon grâce à la fonction rectangle, le balcon à un nombre défini de barreaux, et se place au niveau des porte-fenetres.

### toit

On créer un rectangle noir en guise de toit pour chaque immeuble.

### immeuble

On créer l'immeuble , avec une nombre compris entre 2 et 5 étages. Il a une porte au rez-de-chaussée et 2 fenetres puis chaque étages
à une chance d'avoir une ou plusieurs porte-fenetres accompagné de son balcon. Enfin un toit est placé au dessus du denier étage.

### rue

Enfin, on créer la rue en plaçant entre 3 et 5 immeubles dans la rue.