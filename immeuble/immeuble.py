import turtle as trt
import random
import typing as ty

coord: ty.Tuple[float, float]

# dimensions constantes
MIN_ETAGES = 2
MAX_ETAGES= 5
TAILLE_IMMEUBLE = 140
TAILLE_ETAGE = 60
TAILLE_FENETRE = (30, 30)
TAILLE_PORTE = (30, 45)
TAILLE_BALCON = (30, 50)
TAILLE_PORTE_FENETRE = (30, 30)
BARREAUX = 5
X_FENETRES = (10, 55, 95)
TOIT = (160, 25)
HAUTEUR_TOIT = 45
PROBA_BALCON = 4 
COORD_DEPART = (-450, -380)
IMMEUBLES_MIN = 3
IMMEUBLES_MAX = 5

# Couleurs


COULEUR_FENETRE = 'silver'
COULEUR_PORTE =  'grey'
COULEUR_FACADE = {'coral', 'seagreen', 'peru', 'orchid', 'darkturquoise', 'palevioletred', 'wheat'}
COULEUR_TOIT = 'black'

def va(coord) -> None:
    '''
    Déplace le crayon aux coordonées 


    '''
    trt.up()
    trt.goto(coord)
    trt.down()

def rectangle(taille: ty.Tuple[float, float], couleur, pos: ty.Tuple[float, float] = trt.pos()) -> None:
    '''
    Dessine un rectangle de taille donnée : taille ,et aux coordonnonées : pos

    '''
    va(pos)
    trt.setheading(0)
    trt.color('black', couleur)
    trt.begin_fill()
    for cote in range(4):
        trt.forward(taille[cote % 2])
        trt.left(90)
    trt.end_fill()

def fenetre(coord = trt.pos()) -> None:
    '''
    Dessine une fenetre aux coordonnees : coord , et de taille fixe 

    
    '''
    rectangle(TAILLE_FENETRE, COULEUR_FENETRE, coord)


def porte(coord = trt.pos()) -> None:
    '''
    Dessine une porte aux coordonneés : coord

    
    '''
    
    rectangle(TAILLE_PORTE, COULEUR_PORTE, coord)



def balcon(coord = trt.pos()) -> None:
    '''
    Dessine un balcon de 5 barreaux sur une porte-fenetre

    
    '''
    rectangle(TAILLE_BALCON, COULEUR_FENETRE, coord)
    trt.pensize(2)
    rectangle(TAILLE_PORTE_FENETRE, COULEUR_FENETRE, coord)
    trt.left(90)
    for a in range(BARREAUX):
        x, y = trt.pos()
        trt.forward(30)
        va((x + TAILLE_BALCON[0] // BARREAUX, y))
    trt.pensize(1)


def toit(coord = trt.pos()) -> None:
    '''
    Dessine le toit de l'immeuble

    
    '''
    rectangle(TOIT, 'black', (coord[0] - 10, coord[1]))


def immeuble(etages: int, color, coord = trt.pos()) -> None:
    '''
    Dessine un immeuble
    
    '''
    va(coord)
    x, y = trt.pos()
    rectangle((TAILLE_IMMEUBLE, TAILLE_ETAGE * etages), color, (x, y))
    possibilites_porte = list(X_FENETRES)
    random.shuffle(possibilites_porte)
    porte((x + possibilites_porte.pop(), y))
    for fen in possibilites_porte:
        fenetre((x + fen, y + 20))
    for etage in range(etages - 1):
        for fen in X_FENETRES:
            trt.up()
            if random.randrange(PROBA_BALCON):
                fenetre((x + fen, y + TAILLE_ETAGE * (etage + 1) + 20))
            else:
                balcon((x + fen, y + TAILLE_ETAGE * (etage + 1)))
            trt.up()
            trt.forward(42)
    toit((x, y + TAILLE_ETAGE * etages))
    va((x, y))

def rue():
    '''
    Dessine les différents immeubles dans une rue
    '''
    colors = list(COULEUR_FACADE)
    random.shuffle(colors)
    trt.speed(100)
    va(COORD_DEPART)
    for a in range(random.randint(IMMEUBLES_MIN, IMMEUBLES_MAX)):
        immeuble(random.randint(MIN_ETAGES, MAX_ETAGES), colors.pop(), trt.pos())
        trt.up()
        trt.setheading(0)
        trt.forward(180)
    trt.hideturtle()
    trt.done()



rue()

